<?php

use App\Http\Controllers\ClienteController;
use App\Http\Controllers\TransactionController;
use App\http\Controllers\ReporteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
    return redirect()->route('operaciones.index');
});

Route::get('/menu', function () {
    return view('front.pages.menu');
})->name('menu');

Route::group(["prefix"=>'reportes','as'=>'reportes.'],function(){
    Route::get('/',[ReporteController::class,'index'])->name('index');
});

Route::group(["prefix"=>'clientes','as'=>'clientes.'],function(){
    Route::get('/',[ClienteController::class,'index'])->name('index');
});

Route::group(["prefix"=>'operaciones','as'=>'operaciones.'],function(){
    Route::get('/',[TransactionController::class,'index'])->name('index');
    Route::match(["get","post"],'/validar-tarjeta/{tarjeta?}/{fallos?}',[TransactionController::class,'validarTarjeta'])->name('validar-tarjeta');
    Route::match(["get","post"],'/operaciones/{card?}',[TransactionController::class,'showMenuOperaciones'])->name('menu');
    Route::get('/balance/{tarjeta}',[TransactionController::class,'getBalance'])->name('balance');
    Route::get('/retiro/{tarjeta}',[TransactionController::class,'hacerRetiro'])->name('retiro');
    Route::post('/validar-retiro',[TransactionController::class,'validarRetiro'])->name('retiro.validar');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
