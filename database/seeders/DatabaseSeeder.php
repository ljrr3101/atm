<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        $mainSeeds = [
            ErrorTypeSeed::class,
            TipoTransaccionSeeder::class,
            BancoSeeder::class,
            ClienteSeeder::class,
        ];
        $this->call($mainSeeds);
        Schema::enableForeignKeyConstraints();

        // \App\Models\User::factory(10)->create();
    }
}
