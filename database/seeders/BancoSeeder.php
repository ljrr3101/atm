<?php

namespace Database\Seeders;

use App\Models\Banco;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class BancoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Banco::truncate();
        $comentario = "B.- BALANCE, R.-Retiro, RT.- Retiro Taquilla, DT.- Depósito Taquilla, TE.- Transferencua Electrónica";

        $dataSeed = [
            ["codigo" => "00007","nombre"=>"BANCO DE GALICIA Y BUENOS AIRES S.A.U."],
            ["codigo" => "00011","nombre"=>"BANCO DE LA NACION ARGENTINA"],
            ["codigo" => "00014","nombre"=>"BANCO DE LA PROVINCIA DE BUENOS AIRES"],
            ["codigo" => "00015","nombre"=>"INDUSTRIAL AND COMMERCIAL BANK OF CHINA"],
            ["codigo" => "00016","nombre"=>"CITIBANK N.A."],
        ];

        foreach($dataSeed as $seed){
            Banco::create($seed);
        }

        Schema::enableForeignKeyConstraints();
    }
}
