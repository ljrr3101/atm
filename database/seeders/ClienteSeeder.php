<?php

namespace Database\Seeders;

use App\Models\Banco;
use App\Models\BancoCliente;
use App\Models\BancoClienteTarjeta;
use App\Models\User;
use App\Models\Cliente;
use Database\Factories\BancoClienteTarjetaFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Cliente::truncate();
        $clientes = Cliente::factory()->count(15)->create();

        $tipoCuenta = ["CUG","CC","CA","CS","CSS"];
        $minBancoId = 1;
        $maxBancoId = Banco::orderByDesc('id')->first('id')->id;


        foreach($clientes as $cliente)
        {
            $username  = str_replace(" ","",strtolower(substr($cliente->nombres,0,1)));
            $username .= str_replace(" ","",strtolower($cliente->apellidos));
            $dataUser  = [
                "username"      =>$username,
                "password"      => bcrypt("origin2021"),
                "cliente_id"    => $cliente->id
            ];
            if(!User::where("username",$username)->first()){
                User::create($dataUser);
            }else{
                $dataUser["username"] .= User::where("username",$username)->count();
            }
            $bancoClienteTarjeta = (new BancoClienteTarjetaFactory())->definition();
            $bancoCliente = [
                "banco_id"          => rand($minBancoId,$maxBancoId),
                "cliente_id"        => $cliente->id,
                "tipo_cuenta"       =>  $tipoCuenta[rand(0,4)],
                "codigo_cuenta"     => $bancoClienteTarjeta["codigo_cuenta"],
                "saldo"             => $bancoClienteTarjeta["saldo"],
            ];

            unset($bancoClienteTarjeta["codigo_cuenta"]);
            unset($bancoClienteTarjeta["saldo"]);

            $bancoCliente = BancoCliente::create($bancoCliente);
            $bancoClienteTarjeta["banco_cliente_id"] = $bancoCliente->id;
            $bct = new BancoClienteTarjeta($bancoClienteTarjeta);
            $bancoCliente->tarjetas()->save($bct);
        }
    }
}
