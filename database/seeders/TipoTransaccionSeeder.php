<?php

namespace Database\Seeders;

use App\Models\TipoTransaccion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class TipoTransaccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        TipoTransaccion::truncate();
        $comentario = "B.- BALANCE, R.-Retiro, RT.- Retiro Taquilla, DT.- Depósito Taquilla, TE.- Transferencua Electrónica";

        $dataSeed = [
            ["codigo" => "B","nombre"=>"Balance"],
            ["codigo" => "R","nombre"=>"Retiro"],
            ["codigo" => "RT","nombre"=>"Retiro en Taquilla"],
            ["codigo" => "DT","nombre"=>"Depósito en Taquilla"],
            ["codigo" => "TE","nombre"=>"Transferencia Electrónica"],
            ["codigo" => "AO","nombre"=>"Acceso a Operaciones"],
        ];

        foreach($dataSeed as $seed){
            TipoTransaccion::create($seed);
        }

        Schema::enableForeignKeyConstraints();
    }
}
