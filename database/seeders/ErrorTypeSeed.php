<?php

namespace Database\Seeders;

use App\Models\ErrorType;
use Illuminate\Database\Seeder;

class ErrorTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ErrorType::truncate();

        $dataSeed = [
            ["codigo" => "E001","descripcion"=>"Tarjeta Bloqueada"],
            ["codigo" => "E002","descripcion"=>"Tarjeta Vencida"],
            ["codigo" => "E003","descripcion"=>"Tarjeta Inválida"],
            ["codigo" => "E004","descripcion"=>"Clave Inválida"],
            ["codigo" => "E005","descripcion"=>"Balance insuficiente"],
        ];

        foreach($dataSeed as $seed){
            ErrorType::create($seed);
        }
    }
}
