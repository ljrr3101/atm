<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class BancoClienteTarjetaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $ping = $this->faker->numberBetween(0,9999);
        if($ping < 10)                      $ping="000$ping";
        if($ping >= 10 && $ping < 100)      $ping="00$ping";
        if($ping >= 100 && $ping < 1000)    $ping="0$ping";
        $data = [
            "codigo"        => $this->faker->creditCardNumber,
            "ping"          => "$ping",
            "expiracion"    => Carbon::create($this->faker->creditCardExpirationDate('Y-m'))->endOfMonth()->format('Y-m-d'),
            "bloqueada"     => "0",
            "cvc"           => $this->faker->numberBetween(100,999),
            "codigo_cuenta" => $this->faker->iban,
            "saldo"         => floatval($this->faker->numberBetween(1500,100000))
        ];
        return $data;

    }
}
