<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ClienteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $gender = ["male","female"];
        $randomGender = rand(0,1);
        $nombres = $this->eliminar_tildes($this->faker->firstName($randomGender));
        $apellidos = $this->eliminar_tildes($this->faker->lastName);
        $fname = e($nombres);
        $lname = e($apellidos);
        $correo = $this->eliminar_tildes(str_replace(" ","",strtolower(e("$fname.$lname@yopmail.com"))));
        $cliente =[
            "dni"           => $this->faker->numberBetween(10000000,70000000),
            "nombres"       => $nombres,
            "apellidos"     => $apellidos,
            "correo"        => $correo,
            "telefono"      => $this->faker->phoneNumber,
            "direccion"      => $this->faker->address,
        ];
        return $cliente;
    }

    private function eliminar_tildes($cadena){

        //Codificamos la cadena en formato utf8 en caso de que nos de errores
        //dump("cadena Original: $cadena");
        //$cadena = utf8_decode(utf8_decode($cadena));
        ///dump("cadena codificada: $cadena");

        //Ahora reemplazamos las letras
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena );

        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );
        return $cadena;
    }

}
