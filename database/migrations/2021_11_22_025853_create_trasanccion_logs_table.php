<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrasanccionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trasanccion_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cliente_banco_tarjeta_id');
            $table->string('tipo_transaccion_codigo');
            $table->string("error_codigo");
            $table->string("detalle");
            $table->timestamp("fecha",3)->useCurrent();
            $table->timestamps();

            $table->foreign('tipo_transaccion_codigo')->references('codigo')->on('tipo_transaccions');
            $table->foreign('error_codigo')->references('codigo')->on('error_types');

            $table->index("cliente_banco_tarjeta_id");
            $table->index("tipo_transaccion_codigo");
            $table->index("error_codigo");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trasanccion_logs');
    }
}
