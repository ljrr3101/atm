<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBancoClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banco_clientes', function (Blueprint $table) {
            $comentario = "CUG.- Cuenta Universal Gratuita Gratuita,CC.- Cuenta Corriente, CA.- Caja de Ahorros, CS.- Cuenta Sueldo, CSS.- Cuenta de la Seguridad Social";
            $tipoCuenta = ["CUG","CC","CA","CS","CSS"];
            $table->id();
            $table->unsignedBigInteger('banco_id')->index();
            $table->unsignedBigInteger('cliente_id')->index();
            $table->enum('tipo_cuenta',$tipoCuenta)->comment($comentario);
            $table->string('codigo_cuenta');
            $table->decimal('saldo',18,2);
            $table->timestamps();

            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->foreign('banco_id')->references('id')->on('bancos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banco_clientes');
    }
}
