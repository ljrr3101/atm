<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBancoClienteTarjetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banco_cliente_tarjetas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('banco_cliente_id');
            $table->string('codigo')->unique();
            $table->string('ping');
            $table->string('cvc',3);
            $table->date('expiracion');
            $table->enum("bloqueada",[0,1]);
            $table->timestamps();

            $table->foreign('banco_cliente_id')->references('id')->on('banco_clientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banco_cliente_tarjetas');
    }
}
