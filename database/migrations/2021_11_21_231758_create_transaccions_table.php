<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaccions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('banco_cliente_tarjeta_id');
            $table->string('tipo_transaccion_codigo');
            $table->decimal('monto',18,2)->nullable();
            $table->timestamp('fecha',3)->useCurrent();
            $table->timestamps();

            $table->foreign('banco_cliente_tarjeta_id')->references('id')->on('banco_cliente_tarjetas');
            $table->foreign('tipo_transaccion_codigo')->references('codigo')->on('tipo_transaccions');

            $table->index(["banco_cliente_tarjeta_id"]);
            $table->index(["tipo_transaccion_codigo"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaccions');
    }
}
