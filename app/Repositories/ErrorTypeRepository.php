<?php

namespace App\Repositories;

use App\Models\ErrorType;

class ErrorTypeRepository extends BaseRepository
{
    public function __construct(ErrorType $errorType)
    {
        parent::__construct($errorType);
    }

}
