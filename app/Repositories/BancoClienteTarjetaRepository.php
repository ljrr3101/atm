<?php

namespace App\Repositories;

use App\Models\BancoClienteTarjeta;
use App\Models\ErrorType;

class BancoClienteTarjetaRepository extends BaseRepository
{

    public function __construct(BancoClienteTarjeta $bct)
    {
        parent::__construct($bct);
    }

    public function getByCodigo(string $codigo)
    {
        return $this->model->where('codigo',$codigo)->first();
    }

    public function getWithBalance(int $id)
    {
        return $this->model->with('cuenta')->find($id);
    }

    public function getCodigoTarjetaFormateado($tarjeta,$separador  ='-'){
        $codigo = $tarjeta->codigo;
        $codigo = substr($codigo,0,4)."-".substr($codigo,4,4)."-".substr($codigo,8,4)."-".substr($codigo,12,4);
        $tarjeta->codigo = $codigo;
        return $tarjeta;
    }
}
