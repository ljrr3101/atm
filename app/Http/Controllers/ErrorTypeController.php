<?php

namespace App\Http\Controllers;

use App\Models\ErrorType;
use App\Repositories\ErrorTypeRepository;
use Illuminate\Http\Request;

class ErrorTypeController extends Controller
{
    private $errorTypeRepository;

    public function __construct(ErrorTypeRepository $errorTypeRepository)
    {
        $this->errorTypeRepository = $errorTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $errors = $this->errorTypeRepository->all(3);

        return response()->json($errors,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $errorType = new ErrorType($data);
        return $this->errorTypeRepository->save($errorType);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->errorTypeRepository->get($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ErrorType $error_type)
    {
        $error_type->fill($request->all());
        $error_type = $this->errorTypeRepository->save($error_type);
        return $error_type;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ErrorType $error_type)
    {
        if(!$error_type)
        {
            return response()->json([
                "code" => 404,
                "message" => "El recurso solicitado no existe",
            ],404);
        }
        $errorType = $this->errorTypeRepository->delete($error_type);
        return $errorType;
    }
}
