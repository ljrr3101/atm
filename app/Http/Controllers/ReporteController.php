<?php

namespace App\Http\Controllers;

use App\Models\Transaccion as Operaciones;


use Illuminate\Http\Request;

class ReporteController extends Controller
{
    public function index(Request $request){
        $operaciones = Operaciones::with('tarjeta')->where('tipo_transaccion_codigo','R')->orderByDesc('id')->paginate(10);
        return view('front.pages.reportes.index',compact('operaciones'));
    }

    
}
