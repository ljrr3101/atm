<?php

namespace App\Http\Controllers;

use App\Events\ErrorLog;
use App\Events\Transaccion;
use Illuminate\Http\Request;
use App\Models\BancoClienteTarjeta;
use App\Repositories\BancoClienteTarjetaRepository;
use Carbon\Carbon;

class TransactionController extends Controller
{
    private $bancoClienteTarjetaRepository;

    public function __construct(BancoClienteTarjetaRepository $bancoClienteTarjetaRepository)
    {
        $this->bancoClienteTarjetaRepository = $bancoClienteTarjetaRepository;
    }

    public function index(){
        if(session()->has('attempFailed')) session()->forget('attempFailed');
        return view('front.pages.cajero.index');
    }

    public function validarTarjeta(Request $request,BancoClienteTarjeta $tarjeta  =null,$fallos = null){
        if($request->method() =="POST"){
            $formulario  = $request->except(["_token",'cardnumber']);
            $tarjeta = $this->bancoClienteTarjetaRepository->getByCodigo($formulario["codigo"]);

        }
        if(!session()->has('attempFailed')){
            session()->put('attempFailed',0);
        }


        if(!$tarjeta)
        {
            session()->flash('error-card-code','Número de tarjeta no existe');
            return back();
        }


        if($tarjeta !== null && $tarjeta->bloqueada == "1")
        {
            event( new ErrorLog($tarjeta->id,"AO","E001","Intento de acceso con tarjeta bloqueada"));
            session()->flash('error-card-code','Su tarjeta se encuentra bloqueada, dirijase a su entidad bancaria para su desbloqueo');
            return back();
        }

        $hoy = Carbon::now();
        $fecha_expira = Carbon::create($tarjeta->expiracion);
        if($fecha_expira->lt($hoy)){
            event( new ErrorLog($tarjeta->id,"AO","E001","Intento de acceso con tarjeta vencida"));
            session()->flash('error-card-code','Su tarjeta se encuentra Vencida, dirijase a su entidada bancaria');
            return back();
        }

        return view('front.pages.cajero.ping',compact("tarjeta"));
    }

    public function showMenuOperaciones (Request $request,$card = null)
    {
        if($request->method()=="POST") {
            $tarjeta = $this->bancoClienteTarjetaRepository->get($request->tarjeta);
            $attemptFailed = session()->get('attempFailed');
            if($tarjeta->ping !== $request->ping){
                $attemptFailed++;
                session()->put('attempFailed',$attemptFailed);
                event(new ErrorLog($tarjeta->id,"R","E004","Intento ingresar con clave erronea"));
                if($attemptFailed <= 3){
                    session()->flash('error-card-code','Clave Invalida');
                    return redirect()->route('operaciones.validar-tarjeta',[$tarjeta->id,$attemptFailed]);
                }else{
                    session()->flash('error-card-code','Su tarjeta ha sido bloqueada por superar el numero mámixmo de intentos');
                    session()->forget('attempFailed');
                    $tarjeta->bloqueada = "1";
                    $this->bancoClienteTarjetaRepository->save($tarjeta);
                    event(new ErrorLog($tarjeta->id,"R","E004","Bloqueo de tarjeta por exceso de intentos de acceso con clave inválida"));
                    return redirect()->route('operaciones.index');
                }

            }
        }

        if($request->method()=="GET") $tarjeta = $this->bancoClienteTarjetaRepository->get($card);


        return view('front.pages.cajero.menu-operacion',compact('tarjeta'));
    }

    public function getBalance(Request $request,$tarjeta){

        $tarjeta = $this->bancoClienteTarjetaRepository->getCodigoTarjetaFormateado($this->bancoClienteTarjetaRepository->getWithBalance($tarjeta));
        if(!session()->has('resultado-operacion'))
        {
            event(new Transaccion($tarjeta,"B",0));
        }

        return view('front.pages.cajero.balance',compact('tarjeta'));
    }

    public function hacerRetiro(Request $request,$tarjeta)
    {
        $tarjeta = $this->bancoClienteTarjetaRepository->getWithBalance($tarjeta);
        return view('front.pages.cajero.retiro',compact('tarjeta'));
    }

    public function validarRetiro(Request $request)
    {
        $tarjeta = $this->bancoClienteTarjetaRepository->getWithBalance($request->tarjeta);
        if(floatval($tarjeta->cuenta->saldo) < floatval($request->monto))
        {
            event(new ErrorLog($tarjeta->id,"R","E005","Monto de retiro mayor al disponible en cuenta"));
            session()->flash('error-retiro','El monto indicado supera el limite disponible');
            return redirect()->route('operaciones.retiro',$tarjeta->id);
        }
        else{
            $tarjeta->cuenta->saldo -= floatval($request->monto);
            $tarjeta->cuenta->save();

            event(new Transaccion($tarjeta,"R",floatval($request->monto)));

            session()->flash('resultado-operacion','Ud ha realizado un retiro satisfactoriamente');
            return redirect()->route('operaciones.balance',$tarjeta->id);
        }
    }



}
