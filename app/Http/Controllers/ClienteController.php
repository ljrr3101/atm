<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index(Request $request){
        $clientes = Cliente::with('cuentabancaria')->with('tarjetas')->with('banco')->orderByDesc('id')->paginate(10);
        return view('front.pages.clientes.index',compact('clientes'));
    }
}
