<?php

namespace App\Http\Livewire\Front\Pages;

use Livewire\Component;

class Menu extends Component
{
    public function render()
    {
        return view('livewire.front.pages.menu');
    }
}
