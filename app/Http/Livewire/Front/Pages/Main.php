<?php

namespace App\Http\Livewire\Front\Pages;

use App\Models\BancoClienteTarjeta;
use Livewire\Component;

class Main extends Component
{
    public $codigoTarjeta,$numeroTarjeta,$tarjeta, $esTarjetaValida,$intentosFallidos,$mensajeError;

    protected $listeners = ["validarTarjeta","limpiar","mensajeError","tarjetaValida","comprobarClave"];

    public function mount(){
        $this->codigoTarjeta = "";
        $this->esTarjetaValida = false;
        $this->intentosFallidos = 0;
        $this->tarjeta=null;
    }

    public function validarTarjeta($numero)
    {
        if(strlen($this->codigoTarjeta) < 19){
            $esTarjetaValida = false;
            $this->codigoTarjeta .= "$numero";
            if((strlen(str_replace("-","",$this->codigoTarjeta)) % 4 == 0) && (strlen(str_replace("-","",$this->codigoTarjeta))<=12)){
                $this->codigoTarjeta .= "-";
            }
        }

        if(strlen($this->codigoTarjeta) === 19)
        {
            $esTarjetaValida = true;
            $this->numeroTarjeta = str_replace("-","",$this->codigoTarjeta);
            if(strlen($this->numeroTarjeta)===16){
                $this->emit('habilitarBusqueda',$this->numeroTarjeta);
            }
        }

    }

    public function mensajeError($mensaje)
    {
        $this->codigoTarjeta ="";
        $this->esTarjetaValida = false;
        $this->mensajeError=$mensaje;
        $this->emitTo('keyboard','inhabilitarBusqueda');
    }

    public function limpiar()
    {
        if(strlen($this->codigoTarjeta) > 0)
            $this->codigoTarjeta = "";
    }


    public function tarjetaValida($tarjeta = [])
    {
        $this->tarjeta = collect($tarjeta);
        if($this->tarjeta->isNotEmpty())
        {
            $esTarjetaValida = true;
            $this->emitSelf('mensajeError','');
        }
    }

    public function comprobarClave($clave)
    {
        if($this->tarjeta["ping"]!==$clave)
        {
            $this->intentosFallidos;
            if($this->intentosFallidos < 4) $this->emitSelf('mensajeError','Clave invalida');
            elseif($this->intentosFallidos===4)
            {
                $this->emit('mensajeError','Clave invalida,su tarejta ha sido bloqueada por exceso de intentos fallidos');
            }
        }
    }



    public function render()
    {
        // $this->tarjeta = BancoClienteTarjeta::with('cuenta')->where('codigo','4916269173573037')->first();
        // if($this->tarjeta->bloqueada === "1"){
        //     $this->tarjeta =null;
        // }else{
        //     $this->numeroTarjeta = $this->tarjeta->codigo;
        //     $this->emitTo('keyboard','habilitarBusqueda',$this->numeroTarjeta);
        // }
        return view('livewire.front.pages.main');
    }
}
