<?php

    namespace App\Http\Livewire\Front\Pages;

use App\Models\BancoClienteTarjeta;
use Livewire\Component;

class Keyboard extends Component
{
    public $enabledConfirmButton,$numeroTarjeta,$tarjeta,$ping;
    protected $listeners = ["habilitarBusqueda","inhabilitarBusqueda","buscarTarjeta",'validarPing'];

    public function mount()
    {
        $this->tarjeta = null;
        $this->enabledConfirmButton = false;
        $this->pnf = null;
    }

    public function habilitarBusqueda($numeroTarjeta)
    {

        $this->numeroTarjeta = $numeroTarjeta;
        $this->enabledConfirmButton = true;
    }

    public function validarPing($clave)
    {
        $this->enabledConfirmButton = true;
        dump($this->tarjeta->ping === $clave);
    }

    public function inhabilitarBusqueda()
    {
        $this->numeroTarjeta = "";
        $this->enabledConfirmButton = false;
    }

    public function buscarTarjeta($numeroTarjeta)
    {

        if(str_replace("-","",$numeroTarjeta) === ""){
            $this->emitUp('mensajeError','Debe indicar un número de tarjeta');
        }else{

            $this->tarjeta = BancoClienteTarjeta::with('cuenta')->where('codigo',$this->numeroTarjeta)->first();

            if(!$this->tarjeta)
            {
                $this->emitUp('mensajeError','Número de Tarjeta Inválido');
            }
            else
            {
                if($this->tarjeta->bloqueada === "0") $this->emitUp('tarjetaValida',$this->tarjeta);
                if($this->tarjeta->bloqueada === "1") $this->emitUp('mensajeError','Esta tarjeta se encuentra bloqueada para transacciones');
            }
        }

    }

    public function render()
    {
        return view('livewire.front.pages.keyboard');
    }
}
