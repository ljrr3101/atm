<?php

namespace App\Http\Livewire\Front\Pages;

use Livewire\Component;

class Tarjeta extends Component
{
    public function render()
    {
        return view('livewire.front.pages.tarjeta');
    }
}
