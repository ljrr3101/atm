<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ErrorLog
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $data;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($tarjetaId,$tipoOPeracion,$error,$descripcion)
    {
        $this->data = [
            "cliente_banco_tarjeta_id" => $tarjetaId,
            "tipo_transaccion_codigo" => $tipoOPeracion,
            "error_codigo" => $error,
            "detalle" => $descripcion,
        ];
    }

}
