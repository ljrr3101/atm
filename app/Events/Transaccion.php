<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Transaccion
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $data,$tipoEvento,$monto;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data,$tipoEvento,$monto = 0)
    {
        $this->data = $data;
        $this->tipoEvento = $tipoEvento;
        $this->monto = $monto;
    }

}
