<?php

namespace App\Listeners;

use App\Events\Transaccion;
use App\Http\Controllers\TransactionController;
use App\Models\Transaccion as TransaccionOperacion;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TransaccionSatisfactoria
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Transaccion  $event
     * @return void
     */
    public function handle(Transaccion $event)
    {
        $dataTransaccion = [
            "banco_cliente_tarjeta_id" => $event->data->id,
            "tipo_transaccion_codigo" => $event->tipoEvento,
            "monto" => floatval($event->monto)
        ];

        TransaccionOperacion::create($dataTransaccion);

    }
}
