<?php

namespace App\Listeners;

use App\Events\ErrorLog;
use App\Models\TrasanccionLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuccessRegisterErrorLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ErrorLog  $event
     * @return void
     */
    public function handle(ErrorLog $event)
    {
        TrasanccionLog::create($event->data);
    }
}
