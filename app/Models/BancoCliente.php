<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BancoCliente extends Model
{
    use HasFactory;

    public function tarjetas()
    {
        return $this->hasOne(BancoClienteTarjeta::class,'banco_cliente_id');
    }

    public function hasTarjeta()
    {
        return $this->tarjetas->first() !== null;
    }
}
