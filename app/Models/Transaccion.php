<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaccion extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function tarjeta()
    {
        return $this->belongsTo(BancoClienteTarjeta::class,'banco_cliente_tarjeta_id');
    }

    public function cuenta()
    {
        return $this->tarjeta()->first()->with('cuenta');
    }





}
