<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    public function cuentabancaria()
    {
        return $this->hasOne(BancoCliente::class,'cliente_id');
    }

    public function banco()
    {
        return $this->belongsToMany(Banco::class,'banco_clientes','cliente_id','banco_id');
    }



    public function tarjetas()
    {
        return $this->cuentabancaria()->with('tarjetas');
    }

    public function nombreCompleto()
    {
        return $this->nombres." ".$this->apellidos;
    }
}
