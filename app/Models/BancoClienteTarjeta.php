<?php

namespace App\Models;

use App\Models\Transaccion as TransaccionOperacion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BancoClienteTarjeta extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function cuenta()
    {
        return $this->belongsTo(BancoCliente::class,'banco_cliente_id');
    }

    public function datosCuenta()
    {
        return $this->cuenta->first();
    }

    public function operaciones()
    {
        return $this->hasMany(TransaccionOperacion::class,'banco_cliente_tarjeta_id');
    }

    public function getUltimaOperacion()
    {
        return $this->operaciones()->orderByDesc('id')->first();
    }


}
