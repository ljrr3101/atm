<div class="col-span-4 lg:col-span-5 bg-yellow-300">
    <div class="h-full grid grid-rows-5">
        <div class="row-span-3 w-11/12 lg:w-3/4 mx-auto h-5/6 my-auto bg-white">
            <h4 class="text-3xl text-center py-3 w-full bg-white shadow-lg">
                ATM Origin Software
            </h4>
            <div class="w-full h-full bg-white rounded-lg shadow-lg flex flex-col justify-center items-center">
                <div class="w-full h-full py-4">
                    <div class="h-full rounded-lg bg-white shadow-lg flex flex-col justify-center items-center px-5">
                        <input type="text" id="hdncardnumber" value="4916269173573037">
                        {{-- {{ $tarjeta }} --}}
                        @if (!$tarjeta)
                            <label class="font-semibold w-5/6">Indique su número de tarjeta</label>
                            <div class="w-5/6">
                                {{ strlen($codigoTarjeta) }} - {{ $codigoTarjeta }}
                                <input type="text" id="cardnumber" wire:model="codigoTarjeta" onkeyup="this.value = validateCardNumber(event,this)" onchange="window.livewire.emit('validarTarjeta',this.value)" pattern="[0-9-]" class="rounded-xl w-full py-8 text-3xl">
                                @if ($mensajeError)
                                <span class="text-red-700 text-xl">{{ $mensajeError }}</span>
                                @endif
                            </div>
                        @else
                            <label class="font-semibold w-5/6">Escriba su Clave Secreta</label>
                            <input type="text" id="ping" wire:model="ping"  maxlength="4" onkeyup="this.value = soloNumeros(event,this)" onchange="window.livewire.emit('validarTarjeta',this.value)" pattern="[0-9-]" class="rounded-xl w-full py-8 text-3xl">
                            @if ($mensajeError)
                            <span class="text-red-700 text-xl">{{ $mensajeError }}</span>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row-span-2 mx-auto w-5/6 lg:w-3/4 bg-red-300 h-full">
            @livewire('front.pages.keyboard')
        </div>
    </div>
    <script>

        function soloNumeros(element)
        {

        }

        function validateCardNumber(event,elemento)
        {
            let string = elemento.value;
            const patronReplace = /[a-zA-ZáéíóúñÁÉÍÓÚÑ\s|°¬@"#$&/()='?\\¿¡^´~*¨{\[\]}._:;-]+$/;
            const patron = /[0-9]+$/;

            if(elemento.value.length <19){
                if((event.keyCode >= 48 && event.keyCode<=57)||event.keyCode >= 96 && event.keyCode<=105){
                    let aux = elemento.value.replace(patronReplace,"");
                    aux.replace(patronReplace,"");
                    console.log(`Esto es aux ${aux}`);
                    if(aux.length == 4 || aux.length == 9 || aux.length == 14){
                        string +="-";
                    }
                    if(string.length > 19) string = string.substr(0,19);
                    return string;
                }else{
                    const patronReplace = /[a-zA-ZáéíóúñÁÉÍÓÚÑ\s|°¬@"#$&/()='?\\¿¡^´~*¨{\[\]}._:;-]+$/;
                    if(!patron.test(string)){
                        string = string.replace(patronReplace,"");
                        return string;
                    }
                }
            }

            if(string.length > 19) string = string.substr(0,19);
                    return string;
        }
    </script>
</div>
