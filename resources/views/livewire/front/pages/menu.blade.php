<div class="col-span-2 lg:col-span-1 bg-indigo-300 h-full flex flex-col justify-center">
    {{ $tarjeta }}
    <div class="grid grid-cols-1 gap-4 px-4">
        <button class="rounded-lg bg-gray-300 px-3 py-2 shadow-lg hover:shadow-inner">Retiro</button>
        <button class="rounded-lg bg-gray-300 px-3 py-2 shadow-lg hover:shadow-inner">Balance</button>
        <button class="rounded-lg bg-gray-300 px-3 py-2 shadow-lg hover:shadow-inner">Cambio Clave</button>
        <button class="rounded-lg bg-gray-300 px-3 py-2 shadow-lg hover:shadow-inner">Salir</button>
    </div>
</div>
