<div class="w-full h-full">
    {{-- @if (!$tarjeta) --}}
    @if (1!==1)
    <div class="mx-auto w-5/6 grid grid-cols-6 gap-4 py-5">
        @foreach (range(1, 9) as $item)
            <input type="button"
                class="col-span-2 bg-gray-200 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                wire:click="$emit('validarTarjeta',{{ $item }})" value="{{ $item }}" />
        @endforeach
        <input type="button" wire:click="$emitUp('limpiar')"
            class="col-span-2 bg-red-400 text-white  h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
            value="Limpiar">
        <input type="button" wire:click="$emitUp('validarTarjeta',0)"
            class="col-start-3 col-span-2 bg-gray-200 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
            value="0">

        @if (!$enabledConfirmButton)
            <input type="button" disabled="disabled"
                class="col-span-2 bg-green-300 text-white  h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                value="Aceptar">
        @else
            <input type="button" onclick="buscarTarjeta(document.querySelector('#cardnumber').value)"
                class="col-span-2 bg-green-600 text-white  h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                value="Aceptar">
        @endif
    </div>
    @else
    <div class="mx-auto w-5/6 grid grid-cols-6 gap-4 py-5 ">
        <div class="col-span-5 grid grid-cols-6 gap-4">
        @foreach (range(1, 9) as $item)
            <input type="button"
                class="col-span-2 bg-gray-200 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                wire:click="$emit('validarTarjeta',{{ $item }})" value="{{ $item }}" />
        @endforeach
        <input type="button"
                class="col-span-2 col-start-3 bg-gray-200 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                wire:click="$emit('validarTarjeta',{{ $item }})" value="0" />
        </div>
        <div class="col-span-1 h-full flex flex-col justify-center items-center">
            <div class="grid grid-cols-1 gap-y-4 text-center">
                <input type="button" wire:click="$emitUp('limpiar')"
                    class="bg-red-400 text-white px-2 py-1 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                    value="Salir">
                <input type="button" wire:click="$emitUp('limpiar')"
                    class="bg-yellow-400 text-white px-2 py-1 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                    value="Limpiar">
                <input type="button" id="btnAceptar" disabled="disabled"
                    wire:click="validarPing(document.querySelector('#ping').value)"
                    class=" bg-green-300 text-white px-2 py-1 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                    value="Aceptar">
            </div>
        </div>
    </div>
    @endif
    <script>
        function buscarTarjeta(nroTarjeta) {
            // window.livewire.emit('buscarTarjeta','1234-5678-9032-5896');
            window.livewire.emit('buscarTarjeta', nroTarjeta);
        }

        let ping;

        function setPing(digito) {
            if (document.querySelector("#ping").value.length < 4) {
                if (typeof ping === "undefined") ping = digito
                else ping += digito;
                document.querySelector("#ping").value = ping;
                if (document.querySelector("#ping").value.length == 4) {
                    if (document.querySelector("#btnAceptar").disabled) document.querySelector("#btnAceptar").disabled =
                        false;
                    if (document.querySelector("#btnAceptar").classList.contains('bg-green-300')) {
                        document.querySelector("#btnAceptar").classList.remove('bg-green-300');
                        document.querySelector("#btnAceptar").classList.add('bg-green-600');
                    }
                }
            }

        }
    </script>
</div>
