<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}?{{ substr(time(),-5) }}">

        @livewireStyles

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <style>
            html,body{
                width: 100%;
                height: 100%;
            }

        </style>

    </head>
    <body class="font-sans antialiased">
        <div id="wrapper" class="h-full mx-auto w-11/12 sm:w-5/6 lg:w-3/5 bg-gray-50 overflow-hidden" >
            <div class="bg-white h-16 w-full text-center text-3xl flex flex-col justify-center shadow-lg">
                <h4><i class="fas fa-car-crash    "></i> ATM Origin Software</h4>
            </div>
            @yield('content')
        </div>

        @livewireScripts
        @stack('js')
    </body>
</html>
