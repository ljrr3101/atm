@extends('layouts.app')
@section('content')
    <form class="h-full" action="{{ route('operaciones.menu') }}" method="POST" onreset="clearvalues()">
        @csrf
        <div class="mx-auto w-5/6 h-1/2 py-4">
            <div class="h-full rounded-lg bg-white shadow-lg flex flex-col justify-center items-center px-5">
                {{-- {{ $tarjeta }} --}}
                <label class="font-semibold w-5/6">Marque su clave Secreta</label>
                <div class="w-5/6">
                    <input type="hidden" id="tarjeta" name="tarjeta" value="{{ $tarjeta->id }}">
                    <input type="password" id="ping" name="ping" maxlength="4"
                        class="rounded-xl w-full py-8 text-3xl text-center" data-mask="####-####-####-####"
                        data-mask-clearifnotmatch="true">
                    @if (session()->has('error-card-code'))
                        <span class="text-red-700 text-xl">{{ session()->get('error-card-code') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="mx-auto w-5/6 grid grid-cols-6 gap-4 py-5 h-2/5">
            <div class="col-span-5 grid grid-cols-6 gap-4">
                @foreach (range(1, 9) as $item)
                    <input type="button"
                        class="col-span-2 bg-gray-300 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer hover:bg-gray-900 hover:text-white"
                        onclick="validateCardNumber(event,this)" value="{{ $item }}" />
                @endforeach

                <input type="button" wire:click="$emitUp('validarTarjeta',0)"
                    class="col-start-3 col-span-2 bg-gray-300 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer hover:bg-gray-900 hover:text-white"
                    value="0" onclick="validateCardNumber(event,this)">
            </div>
            <div class="col-span-1 h-full flex flex-col justify-center items-center">
                <div class="grid grid-cols-1 gap-y-5">
                    <input type="button" wire:click="$emitUp('limpiar')"
                        class="bg-red-400 text-white  px-2 py-1 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                        value="Cancelar">
                    <input type="reset" wire:click="$emitUp('limpiar')"
                        class="bg-yellow-400 text-white  px-2 py-1 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                        value="Limpiar">
                        <input type="submit" id="btn_aceptar"
                        class="bg-green-400 text-white  px-2 py-1 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                        value="Aceptar">
                </div>
            </div>
        </div>
    </form>
@endsection
@push('js')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js?{{ substr(microtime(), -5) }}"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js?{{ substr(microtime(), -5) }}">
    </script>
    <script>
        $(document).ready(function() {
            $('#cardnumber').mask('####-####-####-####');
        });

        let ping = "";
        function validateCardNumber(event, elemento) {
            ping+= elemento.value;
            if(ping.length <=4)
            {
                document.querySelector("#ping").value = ping;
                if(ping.length ===16) wire:{
                    document.querySelector("#btn_aceptar").classList.remove('bg-green-300');
                    document.querySelector("#btn_aceptar").classList.add('bg-green-600');
                    document.querySelector("#btn_aceptar").disabled = false;
                }
            }
        }

        function clearvalues(){
            ping = "";
        }


        function maskify(cardcode){
            aux = cardcode.split('');

            if(aux.length %4 == 0)
            {
                if(cardcode.length == 4) aux.splice(aux.length,0,'-');
                if(cardcode.length == 8) {
                    aux.splice(4,0,'-');
                    aux.splice(aux.length,0,'-');
                }
                if(cardcode.length == 12) {
                    aux.splice(4,0,'-');
                    aux.splice(9,0,'-');
                    aux.splice(aux.length,0,'-');
                }

                if(cardcode.length == 16) {
                    aux.splice(4,0,'-');
                    aux.splice(9,0,'-');
                    aux.splice(14,0,'-');
                }

            }
            return aux.join('');
        }
    </script>
@endpush
