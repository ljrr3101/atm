@extends('layouts.app')
@section('content')
<div class="grid grid-cols-4 h-full">
    <div class="col-span-3 h-full">
        <form class="h-full" action="{{ route('operaciones.menu') }}" method="POST" onreset="clearvalues()">
            @csrf
            <div class="mx-auto w-11/12 h-1/2 py-4">
                <div class="h-full rounded-lg bg-white shadow-lg flex flex-col justify-center items-center px-5">
                    @if (session()->has('resultado-operacion'))
                    <div class="px-2 py-1 bg-green-200 border-green-700 shadow-lg w-3/4 text-center">
                        <span class="text-green-700 text-lg">{{ session()->get('resultado-operacion') }}</span>
                    </div>
                    @endif
                    {{-- {{ $tarjeta }} --}}
                    @if (session()->has('resultado-operacion'))
                    <table class="w-full">
                        <tr class="h-16">
                            <th class="text-left w-1/3">Nro de Tarjeta</th>
                            <td>{{ $tarjeta->codigo }}</tr>
                        </tr>
                        <tr class="h-16">
                            <th class="text-left w-1/3">Fecha - Hora de la transaccion</th>
                            <td>{{ \Carbon\Carbon::parse($tarjeta->getUltimaOperacion()->fecha)->format('d-m-Y H:i:s') }}</td>
                        </tr>
                        <tr class="h-16">
                            <th class="text-left w-1/3">Monto Retirado</th>
                            <td>{{ number_format($tarjeta->getUltimaOperacion()->monto,2,",",".") }}</td>
                        </tr>
                        <tr class="h-16">
                            <th class="text-left w-1/3">Saldo Disponible</th>
                            <td>{{ number_format($tarjeta->cuenta->saldo,2,",",".") }}</td>
                        </tr>
                    </table>
                    @else
                    <table class="w-full">
                        <tr class="h-16">
                            <th class="text-left w-1/3">Nro de Tarjeta</th>
                            <td>{{ $tarjeta->codigo }}</tr>
                        </tr>
                        <tr class="h-16">
                            <th class="text-left w-1/3">Fecha Vencimiento</th>
                            <td>{{ \Carbon\Carbon::parse($tarjeta->expiracion)->format('Y/m') }}</td>
                        </tr>
                        <tr class="h-16">
                            <th class="text-left w-1/3">Saldo Disponible</th>
                            <td>{{ number_format($tarjeta->cuenta->saldo,2,",",".") }}</td>
                        </tr>
                    </table>
                    @endif
                </div>
            </div>
            <div class="mx-auto w-11/12 grid grid-cols-6 gap-4 py-5 h-2/5">
                <div class="col-span-6 grid grid-cols-6 gap-4">
                    @foreach (range(1, 9) as $item)
                        <input type="button"
                            class="col-span-2 bg-gray-300 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer hover:bg-gray-900 hover:text-white"
                            onclick="validateCardNumber(event,this)" value="{{ $item }}" />
                    @endforeach

                    <input type="button" wire:click="$emitUp('validarTarjeta',0)"
                        class="col-start-3 col-span-2 bg-gray-300 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer hover:bg-gray-900 hover:text-white"
                        value="0" onclick="validateCardNumber(event,this)">
                </div>
            </div>
        </form>
    </div>
    <div class="col-span-1 h-full flex flex-col justify-center items-center gap-y-5">
        <a href="{{ route('operaciones.menu',$tarjeta->id) }}" class="w-36 text-center px-5 py-3 bg-gray-800 text-white rounded-lg shadow-lg hover:shadow-inner">Atras</a>
        <a href="{{ route('operaciones.index') }}" class="w-36 text-center px-5 py-3 bg-gray-800 text-white rounded-lg shadow-lg hover:shadow-inner">Salir</a>
    </div>
</div>
@endsection
@push('js')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js?{{ substr(microtime(), -5) }}"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js?{{ substr(microtime(), -5) }}">
    </script>
    <script>
        $(document).ready(function() {
            $('#cardnumber').mask('####-####-####-####');
        });

        let ping = "";
        function validateCardNumber(event, elemento) {
            ping+= elemento.value;
            if(ping.length <=4)
            {
                document.querySelector("#ping").value = ping;
                if(ping.length ===16) wire:{
                    document.querySelector("#btn_aceptar").classList.remove('bg-green-300');
                    document.querySelector("#btn_aceptar").classList.add('bg-green-600');
                    document.querySelector("#btn_aceptar").disabled = false;
                }
            }
        }

        function clearvalues(){
            ping = "";
        }


        function maskify(cardcode){
            aux = cardcode.split('');

            if(aux.length %4 == 0)
            {
                if(cardcode.length == 4) aux.splice(aux.length,0,'-');
                if(cardcode.length == 8) {
                    aux.splice(4,0,'-');
                    aux.splice(aux.length,0,'-');
                }
                if(cardcode.length == 12) {
                    aux.splice(4,0,'-');
                    aux.splice(9,0,'-');
                    aux.splice(aux.length,0,'-');
                }

                if(cardcode.length == 16) {
                    aux.splice(4,0,'-');
                    aux.splice(9,0,'-');
                    aux.splice(14,0,'-');
                }

            }
            return aux.join('');
        }
    </script>
@endpush
