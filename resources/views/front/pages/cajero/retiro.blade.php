@extends('layouts.app')
@section('content')
    <form class="h-full" action="{{ route('operaciones.retiro.validar') }}" method="POST" onreset="clearvalues()">
        @csrf
        <div class="mx-auto w-5/6 h-1/2 py-4">
            <div class="h-full rounded-lg bg-white shadow-lg flex flex-col justify-center items-center px-5">
                {{-- {{ $tarjeta }} --}}
                <label class="font-semibold w-5/6">Ingrese el Monto</label>
                <div class="w-5/6">
                    <input type="hidden" id="tarjeta" name="tarjeta" value="{{ $tarjeta->id }}">
                    <input type="text" id="monto" name="monto" maxlength="{{ strlen(intval($tarjeta->cuenta->saldo)) }}"
                        class="rounded-xl w-full py-8 text-3xl text-center" data-mask="####-####-####-####"
                        data-mask-clearifnotmatch="true">
                    @if (session()->has('error-retiro'))
                        <span class="text-red-700 text-xl">{{ session()->get('error-retiro') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="mx-auto w-5/6 grid grid-cols-6 gap-4 py-5 h-2/5">
            <div class="col-span-5 grid grid-cols-6 gap-4">
                @foreach (range(1, 9) as $item)
                    <input type="button"
                        class="col-span-2 bg-gray-300 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer hover:bg-gray-900 hover:text-white"
                        onclick="ingresarMonto(event,this)" value="{{ $item }}" />
                @endforeach

                <input type="button" wire:click="$emitUp('validarTarjeta',0)"
                    class="col-start-3 col-span-2 bg-gray-300 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer hover:bg-gray-900 hover:text-white"
                    value="0" onclick="ingresarMonto(event,this)">
            </div>
            <div class="col-span-1 h-full flex flex-col justify-center items-center">
                <div class="grid grid-cols-1 gap-y-5">
                    <input type="button" wire:click="$emitUp('limpiar')"
                        class="bg-red-400 text-white  px-2 py-1 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                        value="Cancelar">
                    <input type="reset" wire:click="$emitUp('limpiar')"
                        class="bg-yellow-400 text-white  px-2 py-1 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                        value="Limpiar">
                        <input type="submit" id="btn_aceptar"
                        class="bg-green-400 text-white  px-2 py-1 h-10 rounded-lg shadow-lg hover:shadow-inner hover:cursor-pointer"
                        value="Aceptar">
                </div>
            </div>
        </div>
    </form>
@endsection
@push('js')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js?{{ substr(microtime(), -5) }}"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js?{{ substr(microtime(), -5) }}">
    </script>
    <script>
        $(document).ready(function() {
            $('#cardnumber').mask('####-####-####-####');
        });

        let monto = "";
        function ingresarMonto(event, elemento) {
            if(monto.length < document.querySelector("#monto").getAttribute('maxlength'))
            {
                monto+=elemento.value;
                 document.querySelector("#monto").value = monto;
            }
        }

        function clearvalues(){
            monto = "";
        }

    </script>
@endpush
