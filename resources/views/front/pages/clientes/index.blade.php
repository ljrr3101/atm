
@extends('layouts.app')
@section('content')
<div class="w-full h-full my-2 p-4 flex flex-col justify-center items-center">
    <div class="w-full bg-white shadow-lg rounded-lg">
        <div class="h-20 flex flex-row justify-end items-center gap-x-5">
            <a href="{{ route('menu') }}" class="text-center w-20 px-3 py-2 rounded-lg bg-gray-900 text-white">Atrás</a>
            <a href="{{ route('operaciones.index') }}" class="text-center w-20 px-3 py-2 rounded-lg bg-gray-900 text-white">Salir</a>
        </div>
        <div class='h-16 text-center text-lg flex flex-col justify-center bg-blue-300 text-blue-700 font-semibold'>
            <h4>Reporte de operaciones</h4>
        </div>
        <div class="p-4">
            <table class="w-full">
                <thead class="h-10 bg-gray-900 text-white">
                    <tr>
                        <th>#</th>
                        <th>Nombre Completo</th>
                        <th>DNI</th>
                        <th>Banco</th>
                        <th>Tarjeta</th>
                        <th>Saldo</th>
                    </tr>
                    <tbody>
                        @foreach ($clientes as $idx => $item)
                        <tr class="text-center text-xs h-8 {{ $idx%2 == 0 ? 'bg-gray-300' : 'bg-white' }}">
                            <td class="border-r-2">{{ $item->id }}</td>
                            <td class="border-r-2">{{ $item->nombreCompleto() }}</td>
                            <td class="border-r-2">{{ $item->dni }}</td>
                            <td class="border-r-2">{{ $item->banco->first()->nombre }}</td>
                            <td class="border-r-2">{{ $item->cuentabancaria->codigo_cuenta }}</td>
                            <td class="border-r-2">{{ number_format($item->cuentabancaria->saldo,2,".",",") }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </thead>
            </table>
        </div>
        <div class="p-4 h-16">
            {{ $clientes->links() }}
        </div>

    </div>
</div>
@endsection
