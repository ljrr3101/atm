@extends('layouts.app')
@section('content')
    <div class="w-full h-full flex flex-col justify-center items-center gap-y-6">
        <a href="{{ route('operaciones.index') }}" class="w-36 text-center px-5 py-3 bg-gray-800 text-white rounded-lg shadow-lg hover:shadow-inner">Operaciones</a>
        <a href="{{ route('clientes.index') }}" class="w-36 text-center px-5 py-3 bg-gray-800 text-white rounded-lg shadow-lg hover:shadow-inner">Clientes</a>
        <a href="{{ route('reportes.index') }}" class="w-36 text-center px-5 py-3 bg-gray-800 text-white rounded-lg shadow-lg hover:shadow-inner">Reportes</a>
    </div>
@endsection
