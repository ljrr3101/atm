@extends('layouts.app')
@section('content')
<div class="w-full h-full my-2 p-4 flex flex-col justify-center items-center">
    <div class="w-full bg-white shadow-lg rounded-lg">
        <div class="h-20 flex flex-row justify-end items-center gap-x-5">
            <a href="{{ route('menu') }}" class="text-center w-20 px-3 py-2 rounded-lg bg-gray-900 text-white">Atrás</a>
            <a href="{{ route('operaciones.index') }}" class="text-center w-20 px-3 py-2 rounded-lg bg-gray-900 text-white">Salir</a>
        </div>
        <div class='h-16 text-center text-lg flex flex-col justify-center bg-blue-300 text-blue-700 font-semibold'>
            <h4>Reporte de operaciones</h4>
        </div>
        <div class="p-4">
            <table class="w-full">
                <thead class="h-10">
                    <tr>
                        <th>#</th>
                        <th>Tarjeta</th>
                        <th>Fecha Operación</th>
                        <th>Monto</th>
                        <th>Balance</th>
                    </tr>
                    <tbody>
                        @php
                            $retiros = 0;
                        @endphp
                        @foreach ($operaciones as $idx => $item)
                        @php
                            $retiros += $item->monto;
                        @endphp
                        <tr class="h-10 text-center {{ $idx%2 == 0 ? 'bg-gray-300' : 'bg-white' }}">
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->tarjeta->codigo }}</td>
                            <td>{{ \Carbon\Carbon::parse($item->fecha)->format('d-m-Y H:i:s') }}</td>
                            <td class="text-red-600"> - {{ number_format($item->monto,2,".",",") }}</td>
                            <td>{{ number_format($item->tarjeta->cuenta->saldo + $retiros,2,".",",") }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </thead>
            </table>
        </div>
        <div class="p-4 h-16">
            {{ $operaciones->links() }}
        </div>

    </div>
</div>
@endsection
