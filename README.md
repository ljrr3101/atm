<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
Release 1.0.0.0
</p>

# Aplicacion ATM
## Objetivo General
- Desarrollar una aplicación en el Framework laravel que permita emular el comportamiento real de un ATM:

## Objetivo Específico
- Evaluar los conocimientos del aspirante a la vacante disponible Desarrollador Backend con .Net


# Acerca de
- El proyecto fue desarrollado con la ultima versión del Framework Laravel para el momento: 8.73.1
- Version de Mysql Usada 5.7.33,
- Patron de Diseño: MVC Propio de Laravel
# Instalación
## Herramientas
- Validar que se disponga de un servidor web con apache, en caso de no poseerlo, se recomienda usar [Laragon](https://laragon.org/download/index.html) que ya contiene todos los elementos requeridos
    - Apache
    - MySql
    - Cliente de base de datos Heidy
- Validar que el Sistema operativo cuente con Git instalado, en caso de no poseerlo se debe [descargar](https://git-scm.com/downloads)
- Validar que el Sistema operativo cuente con el gestor de dependencias de php Composer, en caso de no poseerlo, se debe [descargar](https://getcomposer.org/download/)
- Editor de códdigo en caso de requerirlo, se recomienda el [VSCode](https://code.visualstudio.com/download)
- La creación de Virtual Host, queda a todal discreción del evaluador
- optivamente se recomienda tener instalado Nodejs, en caso de no tenerlo se recomienda [descargarlo](https://nodejs.org/es/download/)

## Pasos de Instalación
- Crear una Base de Datos en Mysql, El nombre para la base de datos queda a criterio del evaluador, se recomienda el nombre ATM
    ### Nota: 
    -   En caso de requerir usuarios de BD específicos para esta prueba, se reccomienda la creación de un usuario con todos los privilegios quedando a discreción nombre de usuario y contraseña así como el nivel de acceso a la BD, sin embargo se recomienda otorgar en este escenario permisos para operaciones DML , DDL y creación de indices dentro de la BD creada.
- Clonar el proyecto de la siguiente [repositorio]()
- Luego de clonar el proyecto, ejecutar en consola el comando: [composer update]()
- Si el evaluador lo prefiere, una ejecutado el comando anteroir, puede correr en la consola el comando [npm install && npm run devb]()
- Ejecutar en consola el comando [php artisan migrate --seed]() para crear las tablas requeridas en el pryecto y prepoblar las tablas generadas
    - Nota: si desea validar que las tablas y los seeders se ejecutaron de manera correcta puede abrir el cliente bd Mysql que disponga y evaluar si las tablas presentes en el diagrama, se encuentran creadas
- Por ultimo como recomendación personal ejecutar los comando [php artisan config:cache](), [php artisan cache:clear](), [php artisan view:cache]() para validar que el framework no tenga nada en la caché del mismo.
- Para correr el proyecto, se puede ejecutar el comando [php artisan serve](), 
- con el servicio de apache en ejecución puiede navegar hasta la carpetap public del proyecto
- crear un virtual host cuyo document Root debe hacer referencia a la carpeta public del proyecto, y agregar un nuevo dns local dentro del archivo Host para el caso de windows, para sistemas basados en Linux validar el directorio o archivo enabled sites

# Del Proyecto
## Navegabilidad

Posee un [menu](http://atm.test/menu) de navegación como acceso directo

- [Operaciones](http://atm.test/menu).
- [Clientes](http://atm-test/clientes).
- [Reportes](http://atm-test/reportes).

## Criterios de Aceptación
Como Criterios de aceptación se espera que la aplicación cumpla con los siguientes requerimientos:

## Modulos del Sistema
### Modulo de Operaciones
#### Funcionalidad: 
- Al desplegar pantalla se requere una caja de texto y un teclado enteramente numérico y adicionalmente un botón que permita limpiar la caja de texto y aceptar para validar si la tarjeta es valida
    - al pulsar los botones del teclado numérico, este debe ir creando el codigo de la tarjeta, y separarlos en bloques de 4 digitos por el simbolo guión (-) 
    - al pulsar el botón limpiar se sebe borrar la caja de texto y dejarla en blanco para escribir un nuevo número de tarjeta
    - al pulsar el botón aceptar, el sistema debe validar que la tarjeta exista, considerandlo siguientes escenarios:
        - Si la tarjeta no existe, notificar al usuario
            - Si la tarjeta existe, Pero esta bloqueada, notificar al usuario
        - SI la tarjeta exite, mostrar una interfaz con una caja de texto, un teclado numérico y tres botones (Salir, Limpiar,Aceptar) para introducir el ping o clave secreta
            - Si usuario pulsa sobre el boton salir, el sistema regresa a la interfez inicial para operaciones por cajero.
            - Si el usuario pulsa sobre el boton limpiar, este debe resetear la caja de texto para la clave
            - Validaciones
                - Si el usuario comete un error al escribir la clave, el sistema permitir solo 4 intentos fallidos
                - si el usuario supera el número de intentos fallidos de clave, la tarjete se debe bloquear e indicar en un mensaje
                - si la clave especificada es la correcta, debe mostrar la interfaz donde se mostrará un menú de acciones
        - Menu Acciones
            - Balance: al ingresar en la opcion balance del menu, el sistema debe mostrar la siguiente información:
                - Número de Tarjeta
                - Fecha de Expiración
                - saldo en cuenta
                - El sistema debe registrar un log de transacción con la fecha y hora del momento que se produce la consulta del balance
            - Retiro: esta accion permite realizar retiros de fondos de la cuenta del tarjeta habiente, para ellos se debe considerar al momento de presionar el botón de aceptar lo siguiente:
                - El si el monto solicitado supera el saldo disponible, no se debe permitir el retiro de fondos, y regresar a la pantalla anterior indicando el mensaje
                - Monto menor o igual que el saldo en cuenta, se debe permitir el retiro de fondos y ejecutar lo siguiente dentro del sistema:
                    - Actualización de saldo en cuenta
                    - Registro de la Transacción de retiro
                    - Mostrar interfas con el resultado de la operación, para ello se debe mostrar lo siguientes datos en pantalla
                        - Mensaje de operación
                        - Número de tarjeta
                        - Fecha y hora de operación
                        - Monto de la transacción
                        - Saldo dispopnible posterior a la transacción
                    - la interfaz de contar con dos botones de acción, 
                        - Atrás: esta acción permitira realizar un nuevo retiro
                        - Salir: esta acción lo saca del sistema y lo regresa a la pantalla de inicio de operaciones en ATM

### Módulo Clientes

- Este modulo solo mostrara la lista de clientes dentro de la red bancaria, con los siguientes datos:
    - Código Cliente
    - Nombre Completo del Cliente
    - DNI del cliente
    - Banco Afilado
    - Tarjeta Asignada por banco
    - Saldo en cuenta

- Botones de acción:
    - Atras: permite regresar al menú
    - Salir: permite navegar hasta la pantalla principal de operaciones por ATM
    

### Módulo Reportes
- Este modulo solo mostrara la lista de clientes dentro de la red bancaria, con los siguientes datos:
    - Código Cliente
    - Nombre Completo del Cliente
    - DNI del cliente
    - Banco Afilado
    - Tarjeta Asignada por banco
    - Saldo en cuenta


